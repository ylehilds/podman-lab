# Podman lab
## Overview
In this lab, you will install Podman and gain some initial understanding of how it works. Then, you will create containers with a basic Dockerfile and with the Buildah tool.

### Prerequisites
CentOS 7 VM with at least 1 CPU, 1 GB RAM, 10 GB storage, and a connection to the Internet.

### Notes
- Podman can be run in rootless mode, but that requires some setup that is outside the scope of this lab. Either do that setup on your own or run all of the commands with `sudo`.
- A lot of this lab is in the monkey-see-monkey-do format; however, there are questions throughout. When you get to a question, try to answer it on your own before reading the answer. Once you're satisfied with your answer, check your work. Then, spend some time thinking through the closing question. Try not to rush through these.
- If you find any issues with this lab or have any ideas for it, please submit a PR. I'm all about improving content.

## Podman basics
To install Podman, run:
```
yum install -y podman
```

Now that Podman is installed, take a look at the images you currently have pulled:
```
podman images
```
If you have Docker images on your machine already, you'll see those listed; otherwise, you'll see an empty list.

> **Question:** _Where are container images stored on your machine? Are Docker containers stored in a different location? Why? What about rootless containers?_
>
> Container images are stored in `/var/lib/containers`. Docker images are stored in `/var/lib/docker`. These are different because Docker made their decision before 2015 when the Open Containers Initiative (OCI), a containers standards body, was formed. This standards body was a collaboration between Docker, Google, Red Hat, SUSE, CoreOS, and others in an effort to standardize the creation and management of containers.
>
> **Think about it:** Rootless containers are stored in each user's home directory in `~/.local/share/containers`. Why do you think this is important?

Let's pull down the latest Fedora container image:
```
podman pull fedora:latest
```

Now run `podman images` to see the pulled image. Your output should be similar to this:
```
$ podman images
REPOSITORY                 TAG      IMAGE ID       CREATED       SIZE
docker.io/library/fedora   latest   f0858ad3febd   5 weeks ago   201 MB
```

This container currently isn't running. We can start it in interactive mode and give ourselves a terminal with:
```
podman run -it fedora /bin/bash
```

> **Question:** _What is currently running in this container?_
>
> As you poke around a bit, you'll find that a lot of the tools that you're used to aren't there.
>
> **Think about it:** What limitations do you see? Why do you think this is the case?

How do we now inspect the running container on the host? In another terminal, run `podman ps` to see the currently running containers. Here's an example with two Fedora containers running:
```
$ podman ps
CONTAINER ID  IMAGE                            COMMAND    CREATED      STATUS          PORTS  NAMES
0237e4d8dc7a  docker.io/library/fedora:latest  /bin/bash  3 hours ago  Up 3 hours ago         youthful_lovelace
7b4245be6353  docker.io/library/fedora:latest  /bin/bash  3 hours ago  Up 3 hours ago         youthful_dijkstra
```

> **Question:** _Try running a few containers (without the `-it` flags) and have them do different things (e.g., print information, perform a calculation, etc.). Now run `podman ps` to see information about them. Do you see it? Why not? Read through the `podman ps --help` documentation and figure out how to find that information._
>
> You have found that the output of `podman ps` only shows the currently running containers. To see information about the containers that have already run, use `podman ps -a`.
>
> **Think about it:** When you run a container without the `-it` flags, they run and exit. Is this desired behavior? Why not have the container remain active in the background after its work is done?

## Building containers
There are many ways to build containers, each with their own benefits. In this section, we will cover two of the most common ways: using the Buildah tool and using a Dockerfile.

In each section, we will build an image with the following:
- Install nginx
- Expose port 80
- Serve the following `index.html` (create that now):
   ```html
   <html>Hello, world!</html>
   ```

### With Buildah
To install Buildah, run:
```
yum install -y buildah 
```

You can see a list of your currently pulled images with:
```
buildah images
```

You can also see your containers with:
```
buildah containers
```

If you're just starting out, this should be an empty list.

> **Question:** _What's the difference between images and containers?_
>
> Images are the container images you have pulled from a registry (`man buildah images` refers to it as "images in local storage"). Containers are "the working containers and their base images" (see `man buildah containers`).

Let's build a new container from the Fedora image.
```
container=$(buildah from fedora)
```

One of the nice features of Buildah is that when you do something, it outputs the name of the thing it created. If you `echo $container`, you'll see the name of the new working container (something like `fedora-working-container`). Saving that output to a variable can save you some headache&nbsp;&mdash;and typing&nbsp;&mdash; as you go forward.

Great, let's run it:
```
buildah run $container /bin/bash
```

Now, you might be thinking to yourself, "Hey! This feels like Podman. I already did this! Why are we even learning this?" Here's a helpful note from [the Buildah documentation](https://github.com/containers/buildah/blob/master/docs/tutorials/01-intro.md):
> Notice we get a new shell prompt because we are running a bash shell inside of the container. It should be noted that `buildah run` is primarily intended for helping debug during the build process. A runtime like runc or a container interface like [CRI-O](https://github.com/kubernetes-sigs/cri-o) is more suited for starting containers in production.

Now that we have that container built, let's run Julia and print the installed version:
```
buildah run $container julia --version
```

That didn't work because Julia isn't installed. You can install it with:
```
buildah run $container -- dnf install -y julia
```

> **Note**: The `--` tells Buildah that you're done with `buildah run` commands and everything that follows are commands you want to have run inside the container.

Now that Julia is installed, we can run it:
```
buildah run $container julia --version
```

> **Question:** What did you just learn?
>
> You were able to install Julia in a container and it stayed as part of the built container. This is significant. Why do you think that is? Is it possible to build a container image step-by-step and then save that output? Absolutely! This is one of the common ways of building a container image. The rest of this section will focus on that.

Let's build the image we described in the section introduction. Let's start out with a new container:
```
container=$(buildah from fedora)
```

The first thing we need to do is install Nginx:
```
buildah run $container -- dnf install -y nginx
```

We then give the command to run the Nginx process:
```
buildah config --cmd "/usr/sbin/nginx -g 'daemon off;'" $container
```

We expose port 80:
```
buildah config --port 80 $container
```

Now we can copy our `index.html` into place:
```
buildah copy $container index.html /usr/share/nginx/html/
```

Before we commit our changes, it's best practice to add some configuration metadata (edit this with your own information):
```
buildah config --created-by "colby.goettel" $container
buildah config --author "Colby Goettel <colby.goettel@gmail.com>" --label name=fedora31-helloworld $container
```

Now that we have all of that in place, let's commit our build:
```
buildah commit $container hw_image
```

Finally, we can start up the container with Podman. When we do this, we need to make sure we expose (publish) port 80:
```
podman run --detach --publish 80:80 --name webserver hw_image
```

You can verify that it's working by opening a browser and going to the host's IP.

Last thing, let's inspect the newly made image:
```
podman inspect hw_image
```

Make sure you kill the container so that it doesn't conflict with the next section:
```
podman kill webserver
```

> **Question:** _Can I take this image and export it so that Kubernetes can run it?_
>
> Yes, you can! If you're familiar with Kubernetes, you'll know that it likes everything in YAML format. To generate it, simply run (where `webserver` is the name of your container (not image)):
>
>      podman generate kube webserver

## With a Dockerfile
A Dockerfile is a series of commands that describe how an image should be built. Let's start out basic. Create a file called `Dockerfile` and add the following (editing the `LABEL` line, of course):
```Dockerfile
FROM fedora:latest
LABEL maintainer="Colby Goettel <colby.goettel@gmail.com>"
```

> **Best practice:** When working with Dockerfiles, they should always be version controlled. Since this is a lab, we're forgoing this for simplicity, but I would be remiss to not say: please, oh please, always version control your images. This will be especially useful when you move onto other tooling or when you are working in a collaborative environment.

A quick note on the syntax before we build it. The `podman build` command takes an optional argument `--tag` so we can name our new image. You then need to provide it with the directory containing our Dockerfile (not the Dockerfile itself; weird, I know). Got it? Great, let's build it:
```
podman build --tag my_fedora:latest .
```

You can verify with `podman images`. Here's mine:
```
$ podman images
REPOSITORY                    TAG      IMAGE ID       CREATED         SIZE
localhost/my_fedora           latest   7a9f91c8884c   2 minutes ago   201 MB
```

So what all can you do with a Dockerfile? There are several instructions (like `FROM` and `LABEL`) that you can use to build your image. Here are a few (you can see the rest in the [Dockerfile documentation](https://docs.docker.com/engine/reference/builder/)):

| Keyword | Purpose |
| --- | --- |
| CMD | Provide a default for executing the container (only one `CMD` is allowed per Dockerfile) |
| COPY | Copy files into the filesystem of the container |
| ENV | Sets environment variables |
| EXPOSE | Expose a port |
| FROM | The base image to use |
| HEALTHCHECK | How to check if the container is still healthy |
| LABEL | Add metadata to an image |
| RUN | Run a given command |
| USER | Specify a user to run the commands as |

With that in mind, let's go ahead and build the same image we did with Buildah. Here's our Dockerfile:
```
FROM fedora:latest
LABEL maintainer="Colby Goettel <colby.goettel@gmail.com>"
RUN dnf install -y nginx
COPY index.html /usr/share/nginx/html/
ENTRYPOINT ["/usr/sbin/nginx","-g","daemon off;"]
EXPOSE 80
```

> **Note:** Don't just copy and paste the Dockerfile code and move on. Spend a minute reading through it and make sure you understand what's happening.

Build it:
```
podman build --tag my_fedora:latest .
```

Now let's run it so we can verify it worked:
```
podman run --detach --publish 80:80 --name webserver_2 my_fedora
```

Open your browser and point it to the host's IP address to verify that everything worked.

> **Question:** _Can I use Buildah to build images from a Dockerfile?_
>
> Absolutely! But it's a bit much for this lab. Check out [the excellent tutorial on it from Buildah](https://github.com/containers/buildah/blob/master/docs/tutorials/03-on-build.md).

### Recap
> **Think about it:** Now that you have spent some time with Dockerfiles and Buildah, what are your impressions of them? What would you consider each of their strengths? Weaknesses?
>
> The References section at the end of this document has some helpful resources to help you in forming better and more informed opinions of these tools.

The images that we just built are pretty heavy. Think about it: does a production container really need a package manager? What about all the other utilities that are on it? Instead of building an image from something like Fedora (with all its added weight), we can build from scratch. Literally: `buildah from scratch`. The scratch image is basically the kernel with an empty layer on top of it. It would be a bit much to cover it in this lab, but if you would like to learn how to build an image from scratch, check out the [Buildah from scratch tutorial](https://github.com/containers/buildah/blob/master/docs/tutorials/01-intro.md#building-a-container-from-scratch).

## References
### Buildah
- [Buildah repository](https://github.com/containers/buildah)
- [Buildah tutorial](https://github.com/containers/buildah/blob/master/docs/tutorials/01-intro.md). The Buildah section was adapted from this tutorial.
- [Other Buildah tutorials](https://github.com/containers/buildah/tree/master/docs/tutorials)

### Dockerfile
- [Writing a Dockerfile](https://rominirani.com/docker-tutorial-series-writing-a-dockerfile-ce5746617cd)

### Kubernetes
- We didn't cover Kubernetes here, but here's a [getting started tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
- [Podman to Kubernetes conversion](https://developers.redhat.com/blog/2019/01/29/podman-kubernetes-yaml/)

### Podman
- [Podman and Buildah for Docker users](https://developers.redhat.com/blog/2019/02/21/podman-and-buildah-for-docker-users/). The Podman basics section used this article as a primary source.

### Registries
- [Using Buildah with registries](https://github.com/containers/buildah/blob/master/docs/tutorials/02-registries-repositories.md)
- [Podman and insecure registries](https://www.projectatomic.io/blog/2018/05/podman-tls/)
